<?php

namespace tests;

use app\models\GitlabRepo;
use Codeception\Test\Unit;
use Exception;

/**
 * GitlabRepoTest contains test casess for gitlab repo model
 * 
 * IMPORTANT NOTE:
 * All test cases down below must be implemented
 * You can add new test cases on your own
 * If they could be helpful in any form
 */
class GitlabRepoTest extends Unit
{
    /**
     * Test case for counting repo rating
     *
     * @return void
     * @throws Exception
     */
    public function testRatingCount()
    {
        $userRepo = $this->make(GitlabRepo::Class, ['forkCount' => 10, 'startCount' => 5]);
        $expectedRating = 11.25;

        $countedRating = $userRepo->getRating();

        $this->assertEquals($expectedRating, $countedRating);
    }

    /**
     * Test case for repo model data serialization
     *
     * @return void
     * @throws Exception
     */
    public function testData()
    {
        $userRepo = $this->make(GitlabRepo::Class, ['name' => 'myRepo', 'forkCount' => 10,
                                                     'startCount' => 5, 'getRating' => 11.25]);
        $expectedData = [
            'name' => 'myRepo',
            'fork-count' => 10,
            'start-count' => 5,
            'rating' => 11.25,
        ];

        $resultData = $userRepo->getData();

        $this->assertEquals($expectedData, $resultData);
    }

    /**
     * Test case for repo model __toString verification
     *
     * @return void
     * @throws Exception
     */
    public function testStringify()
    {
        $userRepo = $this->make(GitlabRepo::Class, ['name' => ' my!@#$%^&*()\'\"1Repo ', 'forkCount' => 10000,
                                                                                               'startCount' => 500]);
        $expectedOutputString = ' my!@#$%^&*()\'\"1Repo                                                       10000 '
                                                                                                         .'⇅  500 ★';

        $formattedString = $userRepo->__toString();

        $this->assertSame($expectedOutputString, $formattedString);
    }
}