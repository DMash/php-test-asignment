<?php

namespace tests;

use app\components\platforms\Bitbucket;
use app\components\platforms\Github;
use app\components\platforms\Gitlab;
use app\components\Searcher;
use app\models\BitbucketRepo;
use app\models\GitlabRepo;
use Codeception\Test\Unit;
use app\models\User;
use app\models\GithubRepo;
use Exception;

/**
 * SearcherTest contains test casess for searcher component
 * 
 * IMPORTANT NOTE:
 * All test cases down below must be implemented
 * You can add new test cases on your own
 * If they could be helpful in any form
 */
class SearcherTest extends Unit
{
    /**
     * Test case for searching via several platforms with valid result
     *
     * @return void
     * @throws Exception
     */
    public function testSearcherValid()
    {
        $searcher = $this->make(Searcher::class);
        $users = array('frog', 'dev00');
        $user1 = new User('frog','frog','github');
        $user2 = new User('dev00','dev','gitlab');
        $user3 = new User('dev01','dev2','bitbucket');
        $repos1 = array(
            new GithubRepo('test', 5, 5, 17),
            new GithubRepo('test2', 12, 7, 6)
        );
        $repos2 = array(new GitlabRepo('test3', 20, 5));
        $repos3 = array(
            new BitbucketRepo('test4', 40, 50),
            new BitbucketRepo('test5', 4, 50),
            new BitbucketRepo('test6', 1, 50)
        );
        $platforms = array(
            $this->make(Github::class, ['findUserInfo' => $user1, 'findUserRepos' => $repos1]),
            $this->make(Gitlab::class, ['findUserInfo' => $user2, 'findUserRepos' => $repos2]),
            $this->make(Bitbucket::class, ['findUserInfo' => $user3, 'findUserRepos' => $repos3])
        );
        $expectedUsersArray = array ($user3, $user3, $user1, $user2, $user1, $user2);

        $resultUsersArray = $searcher->search($platforms, $users);

        $this->assertEquals($expectedUsersArray, $resultUsersArray);
    }

    /**
     * Test case for searching via several platforms with empty result
     *
     * @return void
     * @throws Exception
     */
    public function testSearcherEmptyResult()
    {
        $searcher = $this->make(Searcher::class);
        $users = array('test1', 'test2');
        $platforms = array(
            $this->make(Github::class, ['findUserInfo' => null, 'findUserRepos' => null]),
            $this->make(Gitlab::class, ['findUserInfo' => null, 'findUserRepos' => null]),
            $this->make(Bitbucket::class, ['findUserInfo' => null, 'findUserRepos' => null])
        );

        $resultUsersArray = $searcher->search($platforms, $users);

        $this->assertEmpty($resultUsersArray);
    }
}