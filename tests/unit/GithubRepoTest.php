<?php

namespace tests;

use app\models\GithubRepo;
use Codeception\Test\Unit;
use Exception;

/**
 * GithubRepoTest contains test casess for github repo model
 * 
 * IMPORTANT NOTE:
 * All test cases down below must be implemented
 * You can add new test cases on your own
 * If they could be helpful in any form
 */
class GithubRepoTest extends Unit
{
    /**
     * Test case for counting repo rating
     *
     * @return void
     * @throws Exception
     */
    public function testRatingCount()
    {
        $userRepo = $this->make(GithubRepo::Class, ['forkCount' => 10, 'startCount' => 9, 'watcherCount' => 5]);
        $expectedRating = 9.833333333333334;

        $countedRating = $userRepo->getRating();

        $this->assertEquals($expectedRating, $countedRating);
    }

    /**
     * Test case for repo model data serialization
     *
     * @return void
     * @throws Exception
     */
    public function testData()
    {
        $userRepo = $this->make(GithubRepo::Class, ['name' => 'myRepo', 'forkCount' => 10, 'startCount' => 9,
                                                                        'watcherCount' => 5, 'getRating' => 9.83]);
        $expectedData = [
            'name' => 'myRepo',
            'fork-count' => 10,
            'start-count' => 9,
            'watcher-count' => 5,
            'rating' => 9.83,
        ];

        $resultData = $userRepo->getData();

        $this->assertEquals($expectedData, $resultData);
    }

    /**
     * Test case for repo model __toString verification
     *
     * @return void
     * @throws Exception
     */
    public function testStringify()
    {
        $userRepo = $this->make(GithubRepo::Class, ['name' => ' my!@#$%^&*()\'\"1Repo ', 'forkCount' => 10000,
                                                                        'startCount' => 100, 'watcherCount' => 500]);
        $expectedOutputString = ' my!@#$%^&*()\'\"1Repo                                                       10000 '
                                                                                                 .'⇅  100 ★  500 👁️';

        $formattedString = $userRepo->__toString();

        $this->assertSame($expectedOutputString, $formattedString);
    }
}