<?php

namespace tests;

use app\components\Factory;
use app\interfaces\IPlatform;
use Codeception\Test\Unit;
use Exception;
use LogicException;

/**
 * FactoryTest contains test cases for factory component
 * 
 * IMPORTANT NOTE:
 * All test cases down below must be implemented
 * You can add new test cases on your own
 * If they could be helpful in any form
 */
class FactoryTest extends Unit
{
    /**
     * Test case for successful creating platform component
     *
     * @return void
     * @throws Exception
     */
    public function testCreateSuccess()
    {
        /**
         * The test reveals error with 'gitlab' value.
         */
        $platformFactory = $this->make(Factory::class);
        $valid_platforms = array('gitlab', 'github', 'bitbucket');
        shuffle($valid_platforms);
        $random_platform = $valid_platforms[0];

        $resultPlatform = $platformFactory->create($random_platform);

        $this->assertInstanceOf(IPlatform::class, $resultPlatform);
    }

    /**
     * Test case for failed result while creating platform component
     *
     * @return void
     * @throws Exception
     */
    public function testCreateFail()
    {
        $platformFactory = $this->make(Factory::class);

        $this->expectException(LogicException::class);

        $platformFactory->create('fail');
    }
}