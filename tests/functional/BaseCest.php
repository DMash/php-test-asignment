<?php

/**
 * Base contains test cases for tesing api endpoint 
 * 
 * @see https://codeception.com/docs/modules/Yii2
 * 
 * IMPORTANT NOTE:
 * All test cases down below must be implemented
 * You can add new test cases on your own
 * If they could be helpful in any form
 */
class BaseCest
{
    /**
     * Example test case
     *
     * @return void
     */
    public function cestExamle(\FunctionalTester $I)
    {
        $I->amOnPage([
            'base/api',
            'users' => [
                'kfr',
            ],
            'platforms' => [
                'github',
            ]
        ]);
        $expected = json_decode('[
            {
                "name": "kfr",
                "platform": "github",
                "total-rating": 1.5,
                "repos": [],
                "repo": [
                    {
                        "name": "kf-cli",
                        "fork-count": 0,
                        "start-count": 2,
                        "watcher-count": 2,
                        "rating": 1
                    },
                    {
                        "name": "cards",
                        "fork-count": 0,
                        "start-count": 0,
                        "watcher-count": 0,
                        "rating": 0
                    },
                    {
                        "name": "UdaciCards",
                        "fork-count": 0,
                        "start-count": 0,
                        "watcher-count": 0,
                        "rating": 0
                    },
                    {
                        "name": "unikgen",
                        "fork-count": 0,
                        "start-count": 1,
                        "watcher-count": 1,
                        "rating": 0.5
                    }
                ]
            }
        ]');
        $I->assertEquals($expected, json_decode($I->grabPageSource()));
    }

    /**
     * Test case for api with bad request params
     *
     * @return void
     */
    public function cestBadParams(\FunctionalTester $I)
    {
        $I->amOnPage([
            'base/api',
            'userrs' => [
                'frog',
            ],
            'platformss' => [
                'github',
            ]
        ]);
        $I->seeResponseCodeIs(400);
        $I->see('Bad request: Missing required parameters: users, platforms');
    }

    /**
     * Test case for api with empty user list
     *
     * @return void
     */
    public function cestEmptyUsers(\FunctionalTester $I)
    {
        $I->amOnPage([
            'base/api',
            'users' => [],
            'platforms' => [
                'github', 'gitlab', 'bitbucket',
            ]
        ]);
        $expected = json_decode('[]');
        $I->seeResponseCodeIsSuccessful();
        $I->assertEquals($expected, json_decode($I->grabPageSource()));
    }

    /**
     * Test case for api with empty platform list
     *
     * @return void
     */
    public function cestEmptyPlatforms(\FunctionalTester $I)
    {
        $I->amOnPage([
            'base/api',
            'users' => [
                'frog', 's', 'kfr'
            ],
            'platforms' => []
        ]);
        $expected = json_decode('[]');
        $I->seeResponseCodeIsSuccessful();
        $I->assertEquals($expected, json_decode($I->grabPageSource()));
    }

    /**
     * Test case for api with non empty platform list
     *
     * @return void
     */
    public function cestSeveralPlatforms(\FunctionalTester $I)
    {
        /**
         * In here we have the following problems - gitlab always returns error, bitbucket always returns empty result
         * (despite results in https://api.bitbucket.org/2.0/repositories/test1). So the real check would be assertion of
         * expected json (merging all platforms together in one response). But for now I checked only 2 platforms (with
         * public APIs from which I could get data).
         */
        $I->amOnPage([
            'base/api',
            'users' => [
                'test1',
            ],
            'platforms' => [
                'github',
                'bitbucket',
            ]
        ]);
        $expected = json_decode('[
           {
              "name":"test1",
              "platform":"github",
              "total-rating":1.5,
              "repos":[
        
              ],
              "repo":[
                 {
                    "name":"test1",
                    "fork-count":0,
                    "start-count":3,
                    "watcher-count":3,
                    "rating":1.5
                 }
              ]
           },
           {
              "name":"test1",
              "platform":"bitbucket",
              "total-rating":1.0,
              "repos":[
        
              ],
              "repo":[
                 {
                    "name":"ares",
                    "fork-count":0,
                    "watcher-count":2,
                    "rating":1.0
                 }
              ]
           }
        ]');
        $I->seeResponseCodeIs(200);
        $I->assertEquals($expected, json_decode($I->grabPageSource()));
    }

    /**
     * Test case for api with non empty user list
     *
     * @return void
     */
    public function cestSeveralUsers(\FunctionalTester $I)
    {
        $I->amOnPage([
            'base/api',
            'users' => [
                'test1',
                'test2',
            ],
            'platforms' => [
                'github',
            ]
        ]);
        $expected = json_decode('[
           {
              "name":"test1",
              "platform":"github",
              "total-rating":1.5,
              "repos":[
        
              ],
              "repo":[
                 {
                    "name":"test1",
                    "fork-count":0,
                    "start-count":3,
                    "watcher-count":3,
                    "rating":1.5
                 }
              ]
           },
           {
              "name":"test2",
              "platform":"github",
              "total-rating":1,
              "repos":[
        
              ],
              "repo":[
                 {
                    "name":"testrepo",
                    "fork-count":0,
                    "start-count":2,
                    "watcher-count":2,
                    "rating":1
                 }
              ]
           }
        ]');
        $I->seeResponseCodeIs(200);
        $I->assertEquals($expected, json_decode($I->grabPageSource()));
    }

    /**
     * Test case for api with unknown platform in list
     * It's expected to ignore unknown platform and return empty list in this case (a better scenario when such situation
     * is validated, but let's consider simple variant here).
     *
     * @return void
     */
    public function cestUnknownPlatforms(\FunctionalTester $I)
    {
        $I->amOnPage([
            'base/api',
            'users' => [
                'test1',
            ],
            'platforms' => [
                'test',
            ]
        ]);
        $expected = json_decode('[]');
        $I->seeResponseCodeIs(200);
        $I->assertEquals($expected, json_decode($I->grabPageSource()));
    }

    /**
     * Test case for api with unknown user in list
     * It's expected to ignore unknown platform and return empty list in this case
     *
     * @return void
     */
    public function cestUnknowUsers(\FunctionalTester $I)
    {
        $I->amOnPage([
            'base/api',
            'users' => [
                'asdasdasd',
            ],
            'platforms' => [
                'github',
            ]
        ]);
        $expected = json_decode('[]');
        $I->seeResponseCodeIs(200);
        $I->assertEquals($expected, json_decode($I->grabPageSource()));
    }

    /**
     * Test case for api with mixed (unknown, real) users and non empty platform list
     * It's expected here that unknown user will be ignored and json with data for valid users is returned.
     *
     * @return void
     */
    public function cestMixedUsers(\FunctionalTester $I)
    {
        $I->amOnPage([
            'base/api',
            'users' => [
                'test1',
                'asdasdasd'
            ],
            'platforms' => [
                'github',
            ]
        ]);
        $expected = json_decode('[
           {
              "name":"test1",
              "platform":"github",
              "total-rating":1.5,
              "repos":[
        
              ],
              "repo":[
                 {
                    "name":"test1",
                    "fork-count":0,
                    "start-count":3,
                    "watcher-count":3,
                    "rating":1.5
                 }
              ]
           }
        ]');
        $I->seeResponseCodeIs(200);
        $I->assertEquals($expected, json_decode($I->grabPageSource()));
    }

    /**
     * Test case for api with mixed (github, gitlab, bitbucket) platforms and non empty user list
     * It is expected here that application will simply ignore unknown platform and return result as if it wasn't set.
     *
     * @return void
     */
    public function cestMixedPlatforms(\FunctionalTester $I)
    {
        $I->amOnPage([
            'base/api',
            'users' => [
                'test1',
            ],
            'platforms' => [
                'github',
                'bitbucket',
                'test',
            ]
        ]);
        $expected = json_decode('[
           {
              "name":"test1",
              "platform":"github",
              "total-rating":1.5,
              "repos":[
        
              ],
              "repo":[
                 {
                    "name":"test1",
                    "fork-count":0,
                    "start-count":3,
                    "watcher-count":3,
                    "rating":1.5
                 }
              ]
           },
           {
              "name":"test1",
              "platform":"bitbucket",
              "total-rating":1.0,
              "repos":[
        
              ],
              "repo":[
                 {
                    "name":"ares",
                    "fork-count":0,
                    "watcher-count":2,
                    "rating":1.0
                 }
              ]
           }
        ]');
        $I->seeResponseCodeIs(200);
        $I->assertEquals($expected, json_decode($I->grabPageSource()));
    }
}